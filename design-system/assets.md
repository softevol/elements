---
layout: layouts/design-system/assets.njk
title: Assets
---

{% max_width max_width %}

# {{ title }}

## Logo & favicon

{% responsive %}

{% assets_block containers,"/img/logo.svg", "95", "20", "Logo" %}

{% assets_block containers,"/img/favicons/favicon-310x150.svg", "310", "150", "Favicon" %}

{% assets_block containers,"/img/favicons/favicon-black.svg", "16", "8", "Favicon" %}

{% assets_block containers,"/img/favicons/favicon.svg", "16", "8", "Favicon" %}

{% end_responsive %}

## Buttons

{% responsive %}

{% assets_block containers,"/img/buttons/accordion-down.svg", "95", "20", "Logo" %}

{% assets_block containers,"/img/buttons/accordion-up.svg", "95", "20", "Logo" %}

{% end_responsive %}

{% end_max_width %}