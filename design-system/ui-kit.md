---
layout: layouts/design-system/ui-kit.njk
title: UI-Kit
---

{% max_width max_width %}

# {{ title }}

## Link

### main-menu__link

{% responsive %}

{% ui_kit_block containers,"main-menu__link", "link" %}

{% end_responsive %}

### main-menu__link--current

{% responsive %}

{% ui_kit_block containers,"main-menu__link main-menu__link--current", "link" %}

{% end_responsive %}

## Button

### burger-button

{% responsive %}

{% ui_kit_block containers,"burger-button", "button" %}

{% end_responsive %}

### burger-button--opened

{% responsive %}

{% ui_kit_block containers,"burger-button burger-button--opened", "button" %}

{% end_responsive %}

### accordion__toggle

{% responsive %}

{% ui_kit_block_2 containers %}
{% accordion %}
{% accordion_title accordion, "Rest" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% ui_kit_block_2 containers, "hover" %}
{% accordion %}
{% accordion_title accordion, "Hover" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% ui_kit_block_2 containers, "active" %}
{% accordion %}
{% accordion_title accordion, "Active" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% ui_kit_block_2 containers, "focus" %}
{% accordion %}
{% accordion_title accordion, "Focus" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% end_responsive %}

### accordion__toggle--active

{% responsive %}

{% ui_kit_block_2 containers %}
{% accordion %}
{% accordion_title accordion, "Rest", "active" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% ui_kit_block_2 containers, "hover" %}
{% accordion %}
{% accordion_title accordion, "Hover", "active" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% ui_kit_block_2 containers, "active" %}
{% accordion %}
{% accordion_title accordion, "Active", "active" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% ui_kit_block_2 containers, "focus" %}
{% accordion %}
{% accordion_title accordion, "Focus", "active" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% ui_kit_block_2 containers, "disabled" %}
{% accordion %}
{% accordion_title accordion, "Disabled", "active" %}
{% endaccordion %}
{% endui_kit_block_2 %}

{% end_responsive %}

{% end_max_width responsive %}
