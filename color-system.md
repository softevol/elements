---
layout: layouts/documentation.njk
title: Color System
---

{% max_width max_width %}

# Color System

## Configuration

In data folder (by default `_data/`) create file `colors.json`. There define your project colors:

``` js

{
  "On Background" : {
    "color" : "#0B2239",
    "label" : "#FFFFFF"
  },
  "Primary" : {
    "500": {
      "color" : "#2962FF",
      "label" : "#FFFFFF"
    }
  },
  "Gray" : {
        "100" : {
        "color" : "#F5F5F5",
        "label" : "#000000" 
    },
    "200" : {
        "color" : "#C4C4C4",
        "label" : "#000000" 
    }
  }
}
```

## Design system

In includes folder (by default `_includes/`) create file `layouts/design-system/color-system.njk`, and there include template from `elements` module:

``` md
{ % include "../../../node_modules/elements/_includes/layouts/design-system/color-system.njk" % }
```

At the root directory of project create folder `design-system`, and there create file `color-system.md` :

``` md
  ---
    layout: layouts/design-system/color-system.njk
    title: Color System
  ---
```

## Include design system

You need add `color_block` shortcode with config and styles.

In Eleventy config file (by default `.eleventy.js`) register shortcode:
``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addShortcode("color_block", elements.designSystem.color_block);
  ...
}
```

Create file `css/design-system/color.css.ejs`, and there write next:

``` css
---
permalink: css/design-system/color.css
---

<%- include("../../node_modules/elements/_includes/css/design-system/color.css.ejs"); %>

``` 

Also, you need add `max_width/end_max_width` with config and styles.
Read more about [`max_width`](/responsive-layout/#max_width).


## Usage

You can use colors in your css.ejs files.

``` css
/* main menu link */

.main-menu__link {
  color: <%= colors['On Background'].color%>;
  padding: 4px 10px;
  text-decoration: none;
  border-radius: 4px;
  border: 1px solid transparent;
}

.main-menu__link:hover,
.main-menu__link.hover {
  background-color: <%= colors.Gray['100'].color%>;
  text-decoration: none;
}

.main-menu__link:active,
.main-menu__link.active {
  background-color: <%= colors.Gray['200'].color%>;
  outline: none;
}

.main-menu__link:focus,
.main-menu__link.focus {
  border-color: <%= colors.Primary['500'].color%>;
  outline: none;
}

.main-menu__link:focus:not(:focus-visible) {
  border-color: transparent;
  outline: none;
}

.main-menu__link:focus-visible {
  border-color: <%= colors.Primary['500'].color%>;
  outline: none;
}

```

Generated css:

``` css
/* main menu link */

.main-menu__link {
  color: #0B2239;
  padding: 4px 10px;
  text-decoration: none;
  border-radius: 4px;
  border: 1px solid transparent;
}

.main-menu__link:hover,
.main-menu__link.hover {
  background-color: #F5F5F5;
  text-decoration: none;
}

.main-menu__link:active,
.main-menu__link.active {
  background-color: #C4C4C4;
  outline: none;
}

.main-menu__link:focus,
.main-menu__link.focus {
  border-color: #2962FF;
  outline: none;
}

.main-menu__link:focus:not(:focus-visible) {
  border-color: transparent;
  outline: none;
}

.main-menu__link:focus-visible {
  border-color: #2962FF;
  outline: none;
}

```

{% end_max_width %}

