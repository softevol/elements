// const path = require("path");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const picture = require("./picture.js");
const responsiveJS = require("./responsive.js");
const designSystem = require("./design-system.js");
const accordion = require("./accordion.js");
const cardData = require("./_data/card.json");
const utils = require("./utils.js");
const favicon = require("./favicon.js");

const path = require("path");
const prettier = require("prettier");


module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(syntaxHighlight);
  eleventyConfig.addPassthroughCopy("js/**/*.js");
  eleventyConfig.addPassthroughCopy("css/**/*.css");
  eleventyConfig.addPassthroughCopy("img");

  // design-system 
  eleventyConfig.addShortcode("color_block", designSystem.color_block);
  eleventyConfig.addShortcode("assets_block", designSystem.assets_block);
  eleventyConfig.addShortcode("ui_kit_block", designSystem.ui_kit_block);
  eleventyConfig.addPairedShortcode("ui_kit_block_2", designSystem.ui_kit_block_2);
  eleventyConfig.addShortcode("typography_block", designSystem.typography_block);

  eleventyConfig.addShortcode("max_width", responsiveJS.max_width);
  eleventyConfig.addShortcode("end_max_width", responsiveJS.end_max_width);
  eleventyConfig.addShortcode("responsive", responsiveJS.responsive);
  eleventyConfig.addShortcode("end_responsive", responsiveJS.end_responsive);
  eleventyConfig.addShortcode("container", responsiveJS.container);
  eleventyConfig.addShortcode("end_container", responsiveJS.end_container);
  eleventyConfig.addShortcode("buffer", responsiveJS.buffer);
  eleventyConfig.addShortcode("end_buffer", responsiveJS.end_buffer);

  eleventyConfig.addAsyncShortcode("picture", responsiveJS.picture);
  eleventyConfig.addAsyncShortcode("pictureSvgPng", picture.SvgPng);
  eleventyConfig.addAsyncShortcode("picturePngWebp", picture.PngWebp);
  eleventyConfig.addAsyncShortcode("pictureJpgWebp", picture.JpgWebp);

  eleventyConfig.addAsyncShortcode("card", card);
  eleventyConfig.addShortcode("debug", utils.debug);
  eleventyConfig.addShortcode("buffer", responsiveJS.buffer);
  eleventyConfig.addAsyncShortcode("favicons", favicon.makeFavicons);

  eleventyConfig.addPairedShortcode("accordion", accordion.accordion);
  eleventyConfig.addShortcode("accordion_title", accordion.title);
  eleventyConfig.addPairedShortcode("accordion_panel", accordion.panel);

  async function card(responsiveJSON, src, alt, title, url) {
    responsiveJS.pushGap(this.page, cardData.PADDING*2 + cardData.BORDER*2);
    let card = `<div class="card">
                  <div class="card__inner">
                    <a href="${url}">
                      ${(await responsiveJS.picture.call(this, responsiveJSON, src, alt))}
                      <h3 class="card__title">${title}</h3>
                    </a>
                  </div>
                </div>`;
    responsiveJS.popFromStack(this.page);
    return card;
  }

  eleventyConfig.addTransform("prettier", function(content, outputPath) {
    const extname = path.extname(outputPath);
    if(extname == ".html") {
      const parser = extname.replace(/^./, "");
      //return content;
      return prettier.format(content, { parser });
    }
    return content;
  });

  return {
    markdownTemplateEngine: 'njk'
  };
}

