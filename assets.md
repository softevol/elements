---
layout: layouts/documentation.njk
title: Assets
---

{% max_width max_width %}

# Assets

## Design system

In includes folder (by default `_includes/`) create file `layouts/design-system/assets.njk`, and there include template from `elements` module:

``` md
{ % include "../../../node_modules/elements/_includes/layouts/design-system/assets.njk" % }
```

At the root directory of project create folder `design-system`, and there create file `assets.md` :

``` md
  ---
    layout: layouts/design-system/assets.njk
    title: Assets
  ---
```

## Include design System

You need add `assets_block` shortcode with config and styles.

In Eleventy config file (by default `.eleventy.js`) register shortcode:
``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addShortcode("assets_block", elements.designSystem.assets_block);
  ...
}
```

Create file `css/design-system/assets.css.ejs`, and there write next:

``` css
---
permalink: css/design-system/assets.css
---

<%- include("../../node_modules/elements/_includes/css/design-system/assets.css.ejs"); %>
``` 

Also, you need add `max_width/end_max_width` with config and styles.
Read more about [`max_width`](/responsive-layout/#max_width).


## Usage


`assets_block` have five params:

``` md
{ % assets_block containers, src, width, height, alt % }
```
`containers` - obj with containers data  
`src` - image source  
`width/height` - images width/height  
`alt` - alt text for img 

`assets_block` shortcode need to use with `responsive` shortcode.  
Use `assets_block` in `design-system/assets.md` file:

``` md

## Images

{ % responsive % }

{ % assets_block "/img/picture/image-1.jpg", "268", "151", "Alt" % }
{ % assets_block "/img/picture/image-2.png", "268", "151", "Alt" % }
{ % assets_block "/img/picture/image-3.svg", "268", "151", "Alt" % }

{ % end_responsive % }

```

{% end_max_width %}