const responsiveData = require("./_data/responsive.json");
const maxWidthData = require("./_data/max_width.json");
const typography = require("./_data/typography.js");
const responsive = require("./responsive.js");

const pictureSvgPng = require("./picture.js");
const menu = require("./_data/menu.js");
const designSystem = require("./design-system.js");
const utils = require("./utils.js");
const accordion = require("./accordion.js");

module.exports = {
  elementsData: responsiveData,
  maxWidthData: maxWidthData,
  typography: typography,
  designSystem: designSystem,

  max_width: responsive.max_width,
  end_max_width: responsive.end_max_width,
  buffer : responsive.buffer,
  end_buffer : responsive.end_buffer,
  container: responsive.container,
  end_container: responsive.end_container,
  responsive : responsive.responsive,
  end_responsive : responsive.end_responsive,

  picture : responsive.picture,
  pushGap: responsive.pushGap,
  popFromStack: responsive.popFromStack,
  pictureSvgPng: pictureSvgPng.SvgPng,

  menu: menu,
  accordion: accordion,

  debug: utils.debug
}