const responsive = require('./responsive.json');

module.exports = {
    links : {
        "Typography": "/design-system/typography/",
        "Color System": "/design-system/color-system/",
        "UI Kit": "/design-system/ui-kit/",
        "Markdown": "/design-system/basic-markdown-syntax/",
        "Grid": "/design-system/responsive-layout-grid/",
        "Assets": "/design-system/assets/",
        "Effects": "/design-system/effects/"
    },
    breakpoint: responsive.MEDIA.tablet
}