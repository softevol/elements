const colors = require('./colors.json');

const BACKGROUND_COLOR = colors['Background'].color;
const ON_BACKGROUND_COLOR = colors['On Background'].color;
const ON_SURFACE_COLOR = colors['On Surface']['On Surface'].color;
const ON_SURFACE_SECONDARY_COLOR = colors['On Surface']['On Surface Secondary'].color;
const FONT_FAMILY = 'system-ui, apple-system, sans-serif';

module.exports = {
  body: {
    'default': {
      'margin' : '0',
      'font-family': FONT_FAMILY,
      'color': ON_BACKGROUND_COLOR,
      'background-color': BACKGROUND_COLOR
    }
  },
  h1: {
    'default': {
      'margin' : '0',
      'padding' : `13.44px 8px 13.44px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '32px',
      'color': ON_SURFACE_COLOR
    }
  },
  h2: {
    'default': {
      'margin' : '0',
      'padding' : `11.92px 8px 11.92px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '24px',
      'color': ON_SURFACE_COLOR
    }
  },
  h3: {
    'default': {
      'margin' : '0',
      'padding' : `10.72px 8px 10.72px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '18.72px',
      'color': ON_SURFACE_COLOR
    }
  },
  h4: {
    'default': {
      'margin' : '0',
      'padding' : `13.28px 8px 13.28px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '16px',
      'color': ON_SURFACE_COLOR
    }
  },
  h5: {
    'default': {
      'margin' : '0',
      'padding' : `14.1776px 8px 14.1776px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '13.28px',
      'color': ON_SURFACE_COLOR
    }
  },
  h6: {
    'default': {
      'margin' : '0',
      'padding' : `16.9776px 8px 16.9776px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '10.72px',
      'color': ON_SURFACE_COLOR
    }
  },
  p: {
    'default': {
      'margin' : '0',
      'padding' : `8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '16px',
      'color': ON_SURFACE_COLOR
    }
  },
  '.main-menu__link': {
    'default': {
      'margin' : '0',
      'padding' : `8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-weight': '400',
      'font-size' : '16px',
      'line-height': '24px',
      'letter-spacing': '0',
      'text-decoration': 'none',
      'color': ON_SURFACE_COLOR
    }
  },
  '.label': {
    'default': {
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '16px',
      'line-height': '24px',
      'color': ON_SURFACE_SECONDARY_COLOR
    }
  },
  '.value': {
    'default': {
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '16px',
      'line-height': '24px',
      'color': ON_SURFACE_COLOR
    }
  },
  '.label-small': {
    'default': {
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '14px',
      'line-height': '24px',
      'color': ON_SURFACE_SECONDARY_COLOR
    }
  },
  'code': {
    'default': {
      'font-family': 'monospace !important'    
    }
  }
}
