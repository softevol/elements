---
layout: layouts/documentation.njk
title: Typography
---

{% max_width max_width %}

# Typography

## Include

Create file `css/typography.css.ejs`, and there write next:
``` css
---
permalink: css/typography.css
---

<%- include("../node_modules/elements/_includes/css/typography.css.ejs"); %>
``` 

## Configuration

In data folder (by default `_data/`) create file `typography.js`.
This file can use your project data - by default `json` or `js` with `module.export` in the data folder. In the configuration below I am using color from `colors.json` file.  
There define your typography parameters in module.exports:

``` js
const colors = require('./colors.json');

const COLOR = colors['On Surface']['On Surface'].color;
const PADDING = 8;
const FONT_FAMILY = 'system-ui, apple-system, sans-serif';

module.exports = {
  h1: {
    'default': {
      'margin' : '0',
      'padding' : `13.44px ${PADDING} 13.44px ${PADDING}`,
      'color' : COLOR,
      'font-family': FONT_FAMILY,
      'font-weight': '700',
      'font-size' : '32px',
      'line-height': '150%',
      'word-spacing': '0px',
      'letter-spacing': '0px',
      'text-transform': 'none'
    },
    'phone': {
      'font-size' : '24px',
    },
    'tablet': {
      'font-size' : '26px',
    },
    'laptop': {
      'font-size' : '28px',
    },
    'desktop': {
      'font-size' : '30px',
    }
  }
``` 

Also, in `_data` create file `responsive.json`. There define your media for a project:

``` js
  {
    "MEDIA": {
      "phone": 600,
      "tablet": 905,
      "laptop": 1240,
      "desktop": 1440
    }
  }
```

## Generated Styles

``` css
h1, .h1 {
  margin : 0;
  padding : 13.44px 8px 13.44px 8px;
  color: #2B3133;
  font-family : system-ui, apple-system, sans-serif;
  font-weight : 700;
  font-size : 32px;
  line-height : 150%;
  word-spacing : 0px;
  letter-spacing : 0px;
  text-transform : none;
}

@media (min-width: 600px) {
  h1, .h1 {
    font-size : 24px;
  }
}

@media (min-width: 905px) {
  h1, .h1 {
    font-size : 26px;
  }
}

@media (min-width: 1240px) {
  h1, .h1 {
    font-size : 28px;
  }
}

@media (min-width: 1440px) {
  h1, .h1 {
    font-size : 30px;
  }
}
```

## Usage

``` md
  # Title h1
  <div class="h1">Pseudo title h1</h1>
```

## Chrome equivalent configuration

``` js 

const FONT_FAMILY = '"Times New Roman", serif';
const COLOR = '#000000';
const BACKGROUND_COLOR = '#FFFFFF';

module.exports = {
  body: {
    'default': {
      'margin' : '0',
      'font-family': `${FONT_FAMILY}`
      'color': COLOR,
      'background-color': BACKGROUND_COLOR
    }
  },
  h1: {
    'default': {
      'margin' : '0',
      'padding' : `13.44px 8px 13.44px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '32px',
    }
  },
  h2: {
    'default': {
      'margin' : '0',
      'padding' : `11.92px 8px 11.92px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '24px',
    }
  },
  h3: {
    'default': {
      'margin' : '0',
      'padding' : `10.72px 8px 10.72px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '18.72px',
    }
  },
  h4: {
    'default': {
      'margin' : '0',
      'padding' : `13.28px 8px 13.28px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '16px',
    }
  },
  h5: {
    'default': {
      'margin' : '0',
      'padding' : `14.1776px 8px 14.1776px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '13.28px',
    }
  },
  h6: {
    'default': {
      'margin' : '0',
      'padding' : `16.9776px 8px 16.9776px 8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '10.72px',
    }
  },
  p: {
    'default': {
      'margin' : '0',
      'padding' : `8px`,
      'font-family': `${FONT_FAMILY}`,
      'font-size' : '16px',
    }
  }
}

```

## Include Design System

In includes folder (by default `_includes/`) create file `layouts/design-system/typography.njk`, and there include template from `elements` module:

``` md
{ % include "../../../node_modules/elements/_includes/layouts/design-system/typography.njk" % }
```

At the root directory of project create folder `design-system`, and there create file `typography.md` :

``` md
---
  layout: layouts/design-system/typography.njk
  title: Typography
---
```

Create file `css/design-system/typography.css.ejs`, and there write next:
``` css
---
permalink: css/design-system/typography.css
---

<%- include("../node_modules/elements/css/design-system/typography.css.ejs"); %>
``` 

Also, you need add `buffer/end_buffer` and `max_width/end_max_width` shortcodes with config and styles.

Read more about [`max_width`](/responsive-layout/#max_width) and [`buffer`](/responsive-layout/#buffer).

{% end_max_width %}
