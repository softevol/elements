---
layout: layouts/documentation.njk
title: Accordion
---

{% max_width max_width %}

# Accordion
## Project requirements
### Initial requirements

Implement widget accordion in conformance with:

- [Accordion Design Pattern in WAI-ARIA Authoring Practices 1.1](https://www.w3.org/TR/wai-aria-practices-1.1/#accordion);
- [Web Content Accessibility Guidelines (WCAG) 2.1](https://www.w3.org/TR/WCAG21/);
- Progressive Enhancement Strategy;
- Mobile First;
- softevol layout appoach;
- Modular javascript [(IIFE)](https://developer.mozilla.org/en-US/docs/Glossary/IIFE);
- Testing Protocol.

### Additional requirements

- Option: open tabs independently/only one tab should always be open;
- Option: position of  indicator for accordion (left/right);
- Class naming;
    - [Documentation BEM](https://ru.bem.info/methodology/quick-start/);
    - [Two Dashes Style](https://ru.bem.info/methodology/naming-convention/#%D1%81%D1%82%D0%B8%D0%BB%D1%8C-two-dashes).
- Animated panel's opening and button's rotate;
- One size for outer border and for dividers between accordion items.

### Implementation

- Use flexbox model for browsers that support flexbox and [@support](https://developer.mozilla.org/ru/docs/Web/CSS/@supports);
- use nth-childs css selector for styling.

### QA

- Check in console browser;
- Check by [validator](https://validator.w3.org/);
- Check with different config parameters;
- Check in browser with disabled js;
- Check in browser with enabled js;
- Check in browser that do not support `@support` (using `@support` to add for accordion `display: flex`) (any IE);
- Check in browser that support `@support` (using `@support` to add for accordion `display: flex`);
- Check in IE 5-11;
- Check, using two accordions component;
- Check, using 1 accordion item in component;
- Edge &gt; Inspect &gt; Issues;
- Check, use lighthouse and [pagespeed](https://pagespeed.web.dev/);

## Include
In Eleventy config file (by default `.eleventy.js`) register shortcodes:
``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addPairedShortcode("accordion", elements.accordion.accordion);
  eleventyConfig.addShortcode("accordion_title", elements.accordion.title);
  eleventyConfig.addPairedShortcode("accordion_panel", elements.accordion.panel);
  ...
}
```

Create file `css/accordion/accordion.css.ejs`, and there write next:
``` css
---
permalink: css/accordion/accordion.css
---

<%- include("../../node_modules/elements/_includes/css/accordion/accordion.css.ejs"); %>
<%- include("../../node_modules/elements/_includes/css/accordion/accordion__toggle.css.ejs"); %>
```

Create file `css/accordion/ie5fix.css.ejs`, and there write next:
``` css
---
permalink: css/accordion/ie5fix.css
---

<%- include("../../node_modules/elements/_includes/css/accordion/ie5fix.css.ejs"); %>
```

Create file `js/accordion.js.ejs`, and there write next:
``` js
---
permalink: js/accordion.js
---

<%- include("../node_modules/elements/_includes/js/accordion.js.ejs"); %>
```

Create file `js/utils.js.ejs`, and there write next:
``` js
---
permalink: js/utils.js
---
...
<%- include("../node_modules/elements/_includes/js/utils/get-elements-by-class-name.js"); %>
...
```

Create file `_includes/partials/js-detect.njk`, and there write next:

``` html
{ % include "../../node_modules/elements/_includes/partials/js-detect.njk" % }
``` 

Prepare 4 images with the path `img/buttons`:
- accordion-down.png
- accordion-down.svg
- accordion-up.png
- accordion-up.svg

## Configuration
In data folder (by default `_data/`) create file `accordion.json` with config parameters.

``` json
{
  "multiply": true,
  "padding": 8,
  "title": {
    "padding": 16
  },
  "panel": {
    "padding": 8
  },
  "indicator": {
    "margin": "8",
    "width": 40,
    "height": 40,
    "position": "right"
  }
}
  
```

| Parametr      | Responsible for |
| ----------- | ----------- |
| `multiply`  | How many accordion panels can be opened together (boolean). If `multiply: true`, accordion panels can be opened/closed in any number, independently of each other.  If `multiply: false` - only one panel can be opened. |
| `padding` | Outer gap for accordion component. |
| `title.padding` | Gap for accordion title. |
| `panel.padding` | Gap for accordion panel. |
| `indicator` | Data obj for indicator button. If the object is not defined or empty - the indicator will not be displayed. |
| `indicator.margin` | Margin for indicator button. |
| `indicator.width` | Width for indicator button. |
| `indicator.height` | Height for indicator button. |
| `indicator.position` | Button location in panel (default right). |


Also, in data folder create file `colors.json`. This file will need colors `Border`, `Surface` and `Gray["100"]`, `Gray["200"]`, `Gray["900"]`.


``` json
{
  ...
  "Surface" : {
    "color" : "#FFFFFF",
    "label" : "#000000"
  },
  "Border" : {
    "color" : "#F0F0F0",
    "label" : "#000000"
  },
  "Gray" : {
    "100" : {
        "color" : "#F5F5F5",
        "label" : "#000000" 
    },
  }
  ...
}
```

### Usage

Include the partials, styles and script for the accordion that you have in your template. 

``` html
<!DOCTYPE html>
...
<head>
...
<!-- Accordion -->
<link rel="stylesheet" href="/css/accordion/accordion.css">
<!--[if IE 5]>
<link rel="stylesheet" href="/css/accordion/ie5fix.css">
<![endif]-->
...
{ % include 'partials/js-detect.njk' % }
</head>
<body>
...
<script src="/js/utils.js"></script>
<script src="/js/accordion.js"></script>
</body>
</html>
```

Component accordion have three param:

- `accordion` - data obj with params for shortcode (for shortcode `accordion_title`);
- `title` - accordion title text (for shortcode `accordion_title`, example bellow - "Accordion 1");
- `status` - accordion status. If `status === active`, accordion panel wiil be open (for shortcodes `accordion_title` and `accordion_panel`).

The content for the accordion you just write between the shortcodes.

``` md
{ % accordion % }
{ % accordion_title accordion, "Accordion 1", "active" % }
{ % accordion_panel "active" % }

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{ % endaccordion_panel % }
{ % endaccordion % }
```

{% accordion %}
{% accordion_title accordion, "Accordion 1", "active" %}
{% accordion_panel "active" %}

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{% endaccordion_panel %}
{% endaccordion %}

{% end_max_width %}