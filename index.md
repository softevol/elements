---
layout: layouts/layout.njk
title: 11ty serial experiments
---

{% max_width max_width %}

# {{ title }}

## Design System

{% responsive %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/typography.svg", "typography", "Typography", "/typography/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/color-system.svg", "color-system", "Color System", "/color-system/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/ui-kit.svg", "ui-kit", "UI Kit", "/ui-kit/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/basic-markdown-syntax.svg", "Basic Markdown Syntax", "basic-markdown-syntax", "https://www.markdownguide.org/basic-syntax/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/responsive-layout.svg", "responsive-layout", "Responsive Layout", "/responsive-layout/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/assets.svg", "assets", "Assets", "/assets/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/effects.svg", "effects", "Effects (planned)", "/404/" %}
{% end_container %}

{% end_responsive %}

## Page Navigation

{% responsive %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/skip-to-main-content.svg", "skip-to-main-content", "Skip to Main Content (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/table-of-content.svg", "table-of-content", "Table of Content (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/heading-link.svg", "heading-link", "Heading Link (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/return-to-toc.svg", "return-to-toc", "Return to ToC (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/in-progress.svg", "Change Url Hash on Scrolling", "Change Url Hash on Scrolling (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/link-highlight.svg", "link-highlight", "Link (with fragment highlight) (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/footnote.svg", "footnote", "Footnote (planned)", "/404/" %}
{% end_container %}

{% end_responsive %}

## Page Sections

{% responsive %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/header.svg", "header", "Header", "/header/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/hero-sections.svg", "hero-section", "Hero Section (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/product.svg", "product", "Product (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/testimonials.svg", "testimonials", "Testimonials (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/team.svg", "team", "Team (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/contact.svg", "contact", "Contact Form (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/sidebar.svg", "sidebar", "Sidebar (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/footer.svg", "footer", "Footer (planned)", "/404/" %}
{% end_container %}

{% end_responsive %}

## Widgets

{% responsive %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/accordion.svg", "accordion", "Accordion", "/accordion/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/breadcrumb.svg", "breadcrumb", "Breadcrumb (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/carousel.svg", "carousel", "Carousel (Slide Show) (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/card.svg", "card", "Card", "/card/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/data-grid.svg", "data-grid", "Data Grid (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/dialog.svg", "dialog", "Dialog (Modal) (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/disclosure.svg", "disclosure", "Disclosure (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/feed.svg", "feed", "Feed (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/listbox.svg", "listbox", "Listbox (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/scalable-image.svg", "scalable-image", "Scalable Image (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/table.svg", "table", "Table (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/tabs.svg", "tabs", "Tabs (planned)", "/404/" %}
{% end_container %}

{% end_responsive %}

## Page Examples

{% responsive %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/in-progress.svg", "all components", "All Components (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/landing.svg", "landing", "Landing (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/about.svg", "about", "About (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/pricing.svg", "pricing", "Pricing (planned)", "/404/" %}
{% end_container %}

{% end_responsive %}

## Technical Pages

{% responsive %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/sitemap.svg", "sitemap", "Sitemap (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/404.svg", "404", "404 Not Found (planned)", "/404/" %}
{% end_container %}

{% container containers, "100_50_33_33_33" %}
{% card responsive, "img/in-progress.svg", "Manual", "Manual (planned)", "/404/" %}
{% end_container %}

{% end_responsive %}

{% end_max_width %}
