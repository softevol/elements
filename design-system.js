const responsive = require("./responsive.js");
const picture = require("./picture.js");

function color_block(containerData, theme, color, label) {
let output = `
${responsive.container.call(this, containerData, "100_50_33_33_33")}
<div class="color-block">
<div class="color-block__inner">
<div style="background-color: ${ color };">
<div class="color-block__buffer">
<p class="color-block__label" style="color: ${ label };">
${ theme }
</p>
<div style="padding-top: 40px"></div>
<p class="color-block__hex" style="color: ${ label };">
${ color }
</p>
</div>
</div>
</div>
</div>
${responsive.end_container.call(this)}
`;
return output;
}

function assets_block(containerData, src, width, height, alt) {
let output = `
${responsive.container.call(this, containerData, "100_50_33_33_33")}
<div class="assets-block">
<div class="assets-block__inner">
<div class="assets-block__buffer">
<img src="${src}" width="${width}" height="${height}" alt="${alt}">
</div>
</div>
</div>
${responsive.end_container.call(this)}
`;
return output;
}

function ui_kit_block(containerData, name, type) {
let elem = {
template: '',
class: name,
text: 'Rest'
}

function linkRewrite() {
if(type === 'link') {
elem.template = `<a class="${elem.class}" href="#">${elem.text}</a>`;
} else if (type === 'button') {
elem.template = `<button class="${elem.class}" type="button"></button>`;
}  
}

linkRewrite();

let output = `${responsive.container.call(this, containerData, "100_50_33_33_33")}
<div class="ui-kit-block">
<div class="ui-kit-block__inner">
<div class="ui-kit-block__buffer">
${elem.template}
</div>
</div>
</div>
${responsive.end_container.call(this)}`;

elem.class = name + ' hover';
elem.text = 'Hover';

linkRewrite();

output += `${responsive.container.call(this, containerData, "100_50_33_33_33")}
<div class="ui-kit-block">
<div class="ui-kit-block__inner">
<div class="ui-kit-block__buffer">
${elem.template}
</div>
</div>
</div>
${responsive.end_container.call(this)}`;

elem.class = name + ' active';
elem.text = 'Pressed';

linkRewrite();

output += `${responsive.container.call(this, containerData, "100_50_33_33_33")}
<div class="ui-kit-block">
<div class="ui-kit-block__inner">
<div class="ui-kit-block__buffer">
${elem.template}
</div>
</div>
</div>
${responsive.end_container.call(this)}`;

elem.class = name + ' focus';
elem.text = 'Focused';

linkRewrite();

output += `${responsive.container.call(this, containerData, "100_50_33_33_33")}
<div class="ui-kit-block">
<div class="ui-kit-block__inner">
<div class="ui-kit-block__buffer">
${elem.template}
</div>
</div>
</div>
${responsive.end_container.call(this)}`;

return output;
}

function typography_block(bufferData, el, medias) {
  console.log('bufferData', bufferData);
  console.log('el', el);
  console.log('medias', medias);


  let regExp = /[\s.,%]/g;
  let elementPart1 = ''
  let elementPart2 = ''

  if(el[0] !== '.') {
    elementPart1 = `<${el} style="text-transform: uppercase;" class="${el.replace(regExp, '') }">${el}</${el}>`;
    elementPart2 = `<${el} class="${el.replace(regExp, '') }">${el}</${el}>`;
  } else {
    elementPart1 = `<div style="text-transform: uppercase;" class="${el.replace(regExp, '') }">${el}</div>`;
    elementPart2 = `<div class="${el.replace(regExp, '') }">${el}</div>`;
  }

  let output = `
    ${responsive.buffer.call(this, bufferData)}
    <div class="typography-block">
    <div class="typography-block__element">
      ${elementPart1}
    </div>
    <div class="typography-block__definitions responsive">
      <div class="typography-block__definitions-item responsive">
        <p class="label">Typeface: </p>
        <p class="value">${ medias.default['font-family'] }</p>
      </div>
      <div class="typography-block__definitions-item responsive">
        <p class="label">Weight: </p>
        <p class="value">${ medias.default['font-weight'] }</p>  
      </div>
      <div class="typography-block__definitions-item responsive">
        <p class="label">Case: </p>
        <p class="value">${ medias.default['text-transform'] }</p>  
      </div>
    </div>
    <div class="buffer-x">
      <div class="typography-block__divider"></div>
    </div>
    <div class="typography-block__parameters responsive">
      <div class="typography-block__parameters-item">
        <div style="padding-top: 4px"></div>
        <table class="typography-block__parameters-table">
          <thead>
            <tr>
              <th>
              </th>
              <th>
                ${picture.SvgPng.call("img/icons/desktop.svg", "desktop", "32", "32")}
              </th>
              <th>
                ${picture.SvgPng.call("img/icons/laptop.svg", "laptop", "32", "32")}
              </th>
              <th>
                ${picture.SvgPng.call("img/icons/tablet.svg", "tablet", "32", "32")}
              </th>
              <th>
                ${picture.SvgPng.call("img/icons/phone.svg", "phone", "32", "32")}
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th class="label-small">Font size</th>
              <td class="value">
                ${ medias.phone ? medias.phone['font-size'] : medias.default['font-size'] }
              </td>
              <td class="value">
                ${ medias.tablet ? medias.tablet['font-size'] : medias.default['font-size'] }
              </td>
              <td class="value">
                ${ medias.laptop ? medias.laptop['font-size'] : medias.default['font-size'] }
              </td>
              <td class="value">
                ${ medias.desktop ? medias.desktop['font-size'] : medias.default['font-size'] }
              </td>
            </tr>
            <tr>
              <th class="label-small">Line height</th>
              <td class="value" data-title="Phone">
              ${ medias.phone ? medias.phone['line-height'] : medias.default['line-height'] }
              </td>
              <td class="value" data-title="Tablet">
              ${ medias.tablet ? medias.tablet['line-height'] : medias.default['line-height'] }
              </td>
              <td class="value" data-title="Laptop">
              ${ medias.laptop ? medias.laptop['line-height'] : medias.default['line-height'] }
              </td>
              <td class="value" data-title="Desktop">
              ${ medias.desktop ? medias.desktop['line-height'] : medias.default['line-height'] }
              </td>
            </tr>
            <tr>
              <th class="label-small">Word spacing</th>
              <td class="value">
              ${ medias.phone ? medias.phone['word-spacing'] : medias.default['word-spacing'] }
              </td>
              <td class="value" data-title="Tablet">
              ${ medias.tablet ? medias.tablet['word-spacing'] : medias.default['word-spacing'] }
              </td>
              <td class="value" data-title="Laptop">
              ${ medias.laptop ? medias.laptop['word-spacing'] : medias.default['word-spacing'] }
              </td>
              <td class="value" data-title="Desktop">
              ${ medias.desktop ? medias.desktop['word-spacing'] : medias.default['word-spacing'] }
              </td>
            </tr>
            <tr>
              <th class="label-small">Letter spacing</th>
              <td class="value">
              ${ medias.phone ? medias.phone['letter-spacing'] : medias.default['letter-spacing'] }
              </td>
              <td class="value" data-title="Tablet">
              ${ medias.tablet ? medias.tablet['letter-spacing'] : medias.default['letter-spacing'] }
              </td>
              <td class="value" data-title="Laptop">
              ${ medias.laptop ? medias.laptop['letter-spacing'] : medias.default['letter-spacing'] }
              </td>
              <td class="value" data-title="Desktop">
              ${ medias.desktop ? medias.desktop['letter-spacing'] : medias.default['letter-spacing'] }
              </td>
            </tr>
          </tbody>
        </table>
        <div style="padding-top: 4px"></div>
      </div>
      <div class="typography-block__parameters-item">
        <div class="typography-block__element">
          ${elementPart2}
        </div>
      </div>
    </div>
  </div>
  ${responsive.end_buffer.call(this)}
  `

  return output;
}

function ui_kit_block_2(data, containerData, type = '') {

  const template = `
  ${responsive.container.call(this, containerData, "100_50_33_33_33")}
  <div class="ui-kit-block ${type}">
  <div class="ui-kit-block__inner">
  <div class="ui-kit-block__buffer">
  ${data}
  </div>
  </div>
  </div>
  ${responsive.end_container.call(this)}`;

  return template;
}

module.exports = {
assets_block: assets_block,
color_block: color_block,
ui_kit_block: ui_kit_block,
typography_block: typography_block,
ui_kit_block_2: ui_kit_block_2
}