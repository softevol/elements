---
layout: layouts/documentation.njk
title: Responsive Layout
---

{% max_width max_width %}

# Responsive Layout
A set of containers designed to create a responsive layout including image optimization.

- [max_width](#max_width)
- [buffer](#buffer)
- [responsive](#responsive)
- [container](#container)
- [picture](#picture)

<div id="max_width">

## `max_width`

</div>

Container limiting the width of the content to a given value, taking into account media queries.


### Include
In Eleventy config file (by default `.eleventy.js`) register two shortcodes:
``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addShortcode("max_width", elements.max_width);
  eleventyConfig.addShortcode("end_max_width", elements.end_max_width);
  ...
}
```

Create file `css/responsive/max-width.css.ejs`, and there write next:
``` css
---
permalink: css/responsive/max-width.css
---

<%- include("../../node_modules/elements/_includes/css/responsive/max-width.css.ejs"); %>
``` 

### Configuration
In data folder (by default `_data/`) create file `max_width.json` and set dimentions for `max_width` container.
``` json
{
  "gap": {
    "padding": 8
  },
  "max_width": 980
}
```

### Usage

Shortcode picture have one param:

max_width - obj with data for max_width container (with defined gap/max_width).

Use `max_width` container in `.md` file:

``` md
{ % max_width max_width % }
  # markdown
{ % end_max_width % }
```

### Generated Styles

``` css
  .container {
    max-width: 964px;
    padding: 8px;
    margin: 0 auto;
  }
```

---

<div id="buffer">

## `buffer`

</div>

Wrapper-container, giving a padding for inner content.

### Include
In Eleventy config file (by default `.eleventy.js`) register two shortcodes:
``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addShortcode("buffer", elements.buffer);
  eleventyConfig.addShortcode("end_buffer", elements.end_buffer);
  ...
}
``` 

Create file `css/responsive/buffer.css.ejs`, and there write next:
``` css
---
permalink: css/responsive/buffer.css
---

<%- include("../../node_modules/elements/_includes/css/responsive/buffer.css.ejs"); %>
``` 

### Configuration

In data folder (by default `_data/`) create file `buffer.json`. There define your typography parameters in module.exports:

``` js

{
  "default": {
    "padding": 8
  },
  "card": {
    "padding": 16,
    "phone": {
      "padding": 8
    },
    "tablet": {
      "padding": 12
    },
    "laptop": {
      "padding": 16
    },
    "desktop": { 
      "padding": 0
    }
  }
}

```

The file defines data for two buffers: `buffer-default` and `buffer-card`.

### Usage

Shortcode buffer have two params:

{ % buffer buffer, key % }

`buffer` - obj with data for buffer, with defined paddings.     
`key` (optional) - type of buffer, that will be used. `key` - have to be the key of the `buffer` data object.

Use `buffer` container in `.md` file:

``` md
{ % buffer buffer % }
  # markdown
{ % end_buffer % }

{ % buffer buffer, "card" % }
  # markdown
{ % end_buffer % }
```

### Generated Styles

``` css
.buffer-default {
  padding : 8px;
}

.buffer-card {
  padding : 16px;
}
  
@media (max-width: 600px) {
  .buffer-card {
    padding: 8px;
  }
}

@media (min-width: 600px) and (max-width: 904px) {
  .buffer-card {
    padding: 12px;
  }
}

@media (min-width: 905px) and (max-width: 1239px) {
  .buffer-card {
    padding: 16px;
  }
}

@media (min-width: 1240px) and (max-width: 1439px) {
  .buffer-card {
    padding: 0px;
  }
}
```

---

<div id="responsive">

## `responsive`

</div>

`responsive` shortcode, for wrapping `container` shortcodes.

### Include
In Eleventy config file (by default `.eleventy.js`) register two shortcodes:

``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addShortcode("responsive", elements.responsive);
  eleventyConfig.addShortcode("end_responsive", elements.end_responsive);
  ...
}
``` 

Create file `css/responsive/responsive.css.ejs`, and there write next:

``` css
---
permalink: css/responsive/responsive.css
---

<%- include("../../node_modules/elements/_includes/css/responsive/responsive.css.ejs"); %>
``` 

### Usage

Look in the <a href="#containers-usage">`containers`</a> documentation part.

### Generated Styles

``` css

.responsive { 
  zoom: 1; 
}

.responsive:before,
.responsive:after { 
  content: "";
  display: table; 
}

.responsive:after {
  clear: both; 
}

@supports (display: flex) {
  .responsive::before,
  .responsive::after {
    content: none;
  }
  .responsive {
    display: flex;
    flex-wrap: wrap;
  }
}

``` 

---
<div id="container">

## `container`

</div>

Container, giving a percentage width from parrent.

### Include
In Eleventy config file (by default `.eleventy.js`) register two shortcodes:

``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addShortcode("container", elements.container);
  eleventyConfig.addShortcode("end_container", elements.end_container);
  ...
}
``` 

Create file `css/responsive/container.css.ejs`, and there write next:
``` css
---
permalink: css/responsive/container.css
---

<%- include("../../node_modules/elements/_includes/css/responsive/container.css.ejs"); %>
``` 

### Configuration

In data folder (by default `_data/`) create file `containers.json`. There define your typography parameters in module.exports:

``` js

{
  "100_75_50_25_25": {
    "phone": 100,
    "tablet": 75,
    "laptop": 50,
    "desktop": 25,
    "default": 25,
  },
  "100_80_60_40_20": {
    "phone": 100,
    "tablet": 80,
    "laptop": 60,
    "desktop": 40,
    "default": 20,
  }
}

```

The file defines data for two columns: `100_75_50_25_25` and `100_80_60_40_20`.

<div id="containers-usage">

### Usage

</div>

`container` shortcode need to use with `responsive` shortcode.
Use `container` container in `.md` file:

``` md
{ % responsive % }
  { % container containers, 100_75_50_25_25 % }
    # markdown
  { % end_container % }

  { % container containers, 100_80_60_40_20 % }
    # markdown
  { % end_container % }
{ % end_responsive % }
```

### Generated Styles

``` css

.container-100_75_50_25_25,
.container-100_80_60_40_20 {
  float: left;
}

.container-100_75_50_25_25 {
  width: 100.00%;
}
  
@media (min-width: 600px) {
  .container-100_75_50_25_25 {
    width: 75.00%;
  }
}
  
@media (min-width: 905px) {
  .container-100_75_50_25_25 {
    width: 50.00%;
  }
}
  
@media (min-width: 1240px) {
  .container-100_75_50_25_25 {
    width: 25.00%;
  }
}
  
@media (min-width: 1440px) {
  .container-100_75_50_25_25 {
    width: 25.00%;
  }
}

.container-100_80_60_40_20 {
  width: 100.00%;
}
  
@media (min-width: 600px) {
  .container-100_80_60_40_20 {
    width: 80.00%;
  }
}
  
@media (min-width: 905px) {
  .container-100_80_60_40_20 {
    width: 60.00%;
  }
}
  
@media (min-width: 1240px) {
  .container-100_80_60_40_20 {
    width: 40.00%;
  }
}
  
@media (min-width: 1440px) {
  .container-100_80_60_40_20 {
    width: 20.00%;
  }
}

```
<div id="picture">

## `picture`

</div>

A shortcode for responsive image.

### Include
In Eleventy config file (by default `.eleventy.js`) register shortcode:

``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addAsyncShortcode("picture", elements.picture);
  ...
}
```

Create file `css/responsive/picture.css.ejs`, and there write next:
``` css
---
permalink: css/responsive/picture.css
---

<%- include("../node_modules/elements/_includes/css/responsive/picture.css"); %>
```

### Configuration
In data folder (by default `_data/`) create file `responsive.json` and there define `MEDIA` for your project:

``` json
{
  "MEDIA": {
    "phone": 600,
    "tablet": 905,
    "laptop": 1240,
    "desktop": 1440
  }
}
```

### Usage
Use `piture` container in `.md` file:

``` md
{ % picture responsive, "src", "alt" % }
```

Shortcode picture have three params:

`responsive` - obj with data for project. For picture it will need `MEDIA` part.  
`src` - path to your source image. You can use svg, png or jpg/jpeg formats. Source image have be in high resolution - img plugin cannot upscale your image for output images with a size above.   
`alt` - alt text for img.

{% end_max_width %}
