const path = require("path");
const Image = require("@11ty/eleventy-img");
const responsiveData = require("./_data/responsive.json");
const ConsoleLogger = require("@11ty/eleventy/src/Util/ConsoleLogger");

/**
 * Support operations max
 **/

const SCREENS = responsiveData.SCREEN_WIDTHES;

// Utils function

function compareNumeric(a, b) {
  if (a > b) return 1;
  if (a == b) return 0;
  if (a < b) return -1;
}

// Function - for work with stack

function getWidthStack(page) {
  if (!page.screenWidthStack) {
    page.screenWidthStack = [];
  }
  return page.screenWidthStack;
}

function pushOnWidthStack(page, params) {
  let stack = getWidthStack(page);
  stack.push(params);
}

function pushMaxWidthOnWidthStack(page, maxWidth) {
  pushOnWidthStack(page, {maxWidth: maxWidth});
}

function pushGapOnWidthStack(page, unusedSpaceWidth) {
  if(typeof(unusedSpaceWidth) === 'object') {
    let obj = JSON.parse(JSON.stringify(unusedSpaceWidth)); 
    for (let key in obj) {
      if(key === 'padding') {
        obj[key] *= 2;
      } else if (key === 'phone' || key === 'tablet' || key === 'laptop' || key === 'desktop') {
        obj[key]["padding"] *= 2;
      }
    }
    pushOnWidthStack(page, {gap: obj});
  }
}

function pushAdaptiveOnWidthStack(page, coefficients) {
  pushOnWidthStack(page, { adaptive: coefficients });
}

function popFromWidthStack(page) {
  let stack = getWidthStack(page);
  if (stack.length) {
      stack.length--;
  }
}

// Calc widthes for picture.

function calcWidthes(media, operations) {
  let localWidthes = SCREENS.slice();
  let maxWidthArray = operations.filter(op => op.maxWidth);
  let maxWidth = null;

  if(maxWidthArray.length > 0 && maxWidthArray.length <= 1) {
    maxWidth = maxWidthArray[0].maxWidth;
  } else if ( maxWidthArray.length > 1){
    maxWidth = maxWidthArray.reduce(function(res, obj) {
      return (obj.maxWidth < res.maxWidth) ? obj.maxWidth : res.maxWidth;
    });
  }

  if(maxWidth) {
    for(let i = 0; i < localWidthes.length; i++) {
      if(localWidthes[i] >= maxWidth) {
        localWidthes[i] = maxWidth;
        localWidthes = localWidthes.slice(0, i+1);
      }
    }
  }

  let mediaRange = 0;

  for(width of localWidthes) {
    if(width < 768) {
      mediaRange++;
    }
  }

  let localWidthesProcessed = localWidthes.slice();

  for(let i = 0; i < localWidthesProcessed.length; i++) {
    for(let op of operations) {    
      if(op.gap) {

        let gap = 0;
        if(typeof(op.gap) === 'object') {
          if(localWidthes[i] < media.phone) {
            gap = op.gap.padding;
          } else if(localWidthes[i] < media.tablet && typeof op.gap['phone'] !== "undefined") {
            gap = op.gap.phone.padding;
          } else if(localWidthes[i] < media.laptop && typeof op.gap['tablet'] !== "undefined") {
            gap = op.gap.tablet.padding;
          } else if(localWidthes[i] < media.desktop && typeof op.gap['laptop'] !== "undefined") {
            gap = op.gap.laptop.padding;
          } else if(typeof op.gap['desktop'] !== "undefined") {
            gap = op.gap.desktop.padding;
          } else {
            gap = op.gap.padding;
          }
        }

        localWidthesProcessed[i] -= gap;
      }
  
      if(op.adaptive) {
        if(localWidthes[i] < media.phone) {
          localWidthesProcessed[i] *= op.adaptive.default;
        } else if(localWidthes[i] < media.tablet) {
          localWidthesProcessed[i] *= op.adaptive.phone;
        } else if(localWidthes[i] < media.laptop) {
          localWidthesProcessed[i] *= op.adaptive.tablet;
        } else if(localWidthes[i] < media.desktop) {
          localWidthesProcessed[i] *= op.adaptive.laptop;
        } else {
          localWidthesProcessed[i] *= op.adaptive.desktop;
        }
        localWidthesProcessed[i] = Math.round(localWidthesProcessed[i]);
      }
    }
  }

  maxWidth = localWidthesProcessed[localWidthesProcessed.length - 1];
  let count = localWidthesProcessed.length;

  for(let i = 0; i < count; i++) {
    let width = localWidthesProcessed[i];
    if(i < mediaRange) {
      localWidthesProcessed.push(width*2);
      localWidthesProcessed.push(width*3);
    } else {
      localWidthesProcessed.push(Math.round(width*1.5));
      localWidthesProcessed.push(width*2);
    }
  }

  localWidthesProcessed.sort(compareNumeric);
  // Внутренняя неочевидная логика. Функция всегда выдает последним элементом массива максимальную ширину картинки для фолбека (без коэффициента ретины). 
  // Позже, на основе последнего элемента в функции picture будет нарезан png/jpg фолллбэк для старых браузеров.
  let set = Array.from(new Set(localWidthesProcessed));
  set.splice(set.indexOf(maxWidth), 1);
  set.push(maxWidth);

  return set;
}

function calcSizes(media, operations) {
  let sizesDefault = '100vw';
  let maxWidth = null;
  let sizes = {
    phone: sizesDefault,
    tablet: sizesDefault,
    laptop: sizesDefault,
    desktop: sizesDefault,
    default: sizesDefault
  };

  for(let op of operations) {
    if(op.maxWidth) {
      maxWidth = op.maxWidth;
      sizes.maxWidth = op.maxWidth;
    }

    if(op.adaptive) {
      if(op.adaptive.default < 1) {
        sizes.default = `(${sizes.default} * ${(op.adaptive.default).toFixed(2)})`;
        if (maxWidth && maxWidth >= media.desktop) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.default));
        }
      }

      if(op.adaptive.phone < 1) {
        sizes.phone = `(${sizes.phone} * ${(op.adaptive.phone).toFixed(2)})`;
        if(maxWidth && maxWidth < media.phone) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.phone));
        }
      }

      if(op.adaptive.tablet < 1) {
        sizes.tablet = `(${sizes.tablet} * ${(op.adaptive.tablet).toFixed(2)})`;
        if(maxWidth && maxWidth >= media.phone && maxWidth < media.tablet) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.tablet));
        }
      }
      
      if(op.adaptive.laptop < 1) {
        sizes.laptop = `(${sizes.laptop} * ${(op.adaptive.laptop).toFixed(2)})`;
        if(maxWidth && maxWidth >= media.tablet && maxWidth < media.laptop) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.laptop));
        }
      }
 
      if(op.adaptive.desktop < 1) {
        sizes.desktop = `(${sizes.desktop} * ${(op.adaptive.desktop).toFixed(2)})`;
        if(maxWidth && maxWidth >= media.laptop && maxWidth < media.desktop) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.desktop));
        }
      }
    }

    if(op.gap) {
      if(typeof(op.gap) === 'object') {
        sizes.default = `(${sizes.default} - ${op.gap.padding}px)`;

        if(typeof op.gap['phone'] !== "undefined") {
          sizes.phone = `(${sizes.phone} - ${op.gap.phone.padding}px)`;
        } else {
          sizes.phone = `(${sizes.phone} - ${op.gap.padding}px)`;
        }
  
        if(typeof op.gap['tablet'] !== "undefined") {
          sizes.tablet = `(${sizes.tablet} - ${op.gap.tablet.padding}px)`;
        } else {
          sizes.tablet = `(${sizes.tablet} - ${op.gap.padding}px)`;
        }
  
        if(typeof op.gap['laptop'] !== "undefined") {
          sizes.laptop = `(${sizes.laptop} - ${op.gap.laptop.padding}px)`;
        } else {
          sizes.laptop = `(${sizes.laptop} - ${op.gap.padding}px)`;
        }
  
        if(typeof op.gap['desktop'] !== "undefined") {
          sizes.desktop = `(${sizes.desktop} - ${op.gap.desktop.padding}px)`;
        } else {
          sizes.desktop = `(${sizes.desktop} - ${op.gap.padding}px)`;
        }
      }

      if(sizes.maxWidth) {
        if(maxWidth < media.phone) {
          sizes.maxWidth -= op.gap.padding;
        } else if(maxWidth < media.tablet && typeof op.gap['phone'] !== "undefined") {
          sizes.maxWidth -= op.gap.phone.padding;
        } else if(maxWidth < media.laptop && typeof op.gap['tablet'] !== "undefined") {
          sizes.maxWidth -= op.gap.tablet.padding;
        } else if(maxWidth < media.desktop && typeof op.gap['laptop'] !== "undefined") {
          sizes.maxWidth -= op.gap.laptop.padding;
        } else if(typeof op.gap['desktop'] !== "undefined") {
          sizes.maxWidth -= op.gap.desktop.padding;
        } else {
          sizes.maxWidth -= op.gap.padding;
        }
      }
    }
  }

  let result = '';

  for(let size in sizes) {
    if(size === 'maxWidth') {
      result = `(min-width: ${maxWidth}px) ${Math.round(sizes[size])}px, ` + result;
    }

    if(size  === 'default') {
      result = result + `calc(${sizes[size]})`;
    }

    if(size  === 'phone') {
      result = `(min-width: ${media.phone}px) calc(${sizes[size]}), ` + result;
    }
    
    if(size  === 'tablet') {
      result = `(min-width: ${media.tablet}px) calc(${sizes[size]}), ` + result;
    }
    
    if(size  === 'laptop') {
      result = `(min-width: ${media.laptop}px) calc(${sizes[size]}), ` + result;
    }

    if(size  === 'desktop') {
      result = `(min-width: ${media.desktop}px) calc(${sizes[size]}), ` + result;
    }
  }
  return result;
}

async function picture(responsive, src, alt) {
  let media = responsive.MEDIA;
  const isTEST = false;
  const extension = path.extname(src);
  const name = path.basename(src, extension);
  let operationStack = getWidthStack(this.page);

  let result = '';
  let srcset = calcWidthes(media, operationStack);
  let sizes = calcSizes(media, operationStack);

  if(extension == '.svg') {
    let metadata = await Image(src, {
      svgAllowUpscale: true,
      widths: [srcset[srcset.length - 1]],
      formats: ['png'],
      outputDir: './_site/img',
      filenameFormat: function (id, src, width, format, options) {
        return `${name}-${[srcset[srcset.length - 1]]}w.png`;
      }
    });

    if(isTEST) {
      result = `<picture>
                  <source type="image/svg+xml" srcset="https://picture.softevol.com/svg/image-${metadata.png[0].width}x${Math.round(metadata.png[0].width / 16 * 9)}.svg ${metadata.png[0].width}w">
                  <img src="https://picture.softevol.com/svg/image-${metadata.png[0].width}x${Math.round(metadata.png[0].width / 16 * 9)}.svg" alt="${alt}" width="${metadata.png[0].width}" height="${Math.round(metadata.png[0].width / 16 * 9)}">
                </picture>`
    
    } else {
      result = `<picture>
      <source type="image/svg+xml" srcset="/${src}">
      <img src="${metadata.png[0].url}" alt="${alt}" width="${metadata.png[0].width}" height="${metadata.png[0].height}">
    </picture>`;
    }
  } else if (extension == '.png') {
    let metadata = await Image(src, {
      widths: srcset,
      formats: ['webp'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${name}-${width}w.${format}`;
      }
    });

    let fallbackMetadata = await Image(src, {
      widths: [srcset[srcset.length - 1]],
      formats: ['png'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${name}-${width}w.${format}`;
      }
    });

    if(isTEST) {
      result  = 
      `<picture>
        <source sizes="${sizes}" type="image/svg+xml" srcset="\n`;
      result += metadata['webp'].map(webp => `      https://picture.softevol.com/svg/image-${webp.width}x${Math.round(webp.width / 16 * 9)}.svg ${webp.width}w`).join(",\n");
      result += `">\n`;
      result += `<!-- TODO add img -->\n`;
      result += `  <img src="https://picture.softevol.com/svg/image-${fallbackMetadata.png[0].width}x${Math.round(fallbackMetadata.png[0].width / 16 * 9)}.svg" alt="${alt}" width="${fallbackMetadata.png[0].width}" height="${Math.round(fallbackMetadata.png[0].width / 16 * 9)}">\n`;
      result += `</picture>`;    
    } else {
      let fallbackWidth;
      let fallbackHeight;

      if(srcset[srcset.length - 1] !== fallbackMetadata.png[0].width) {
        fallbackWidth = srcset[srcset.length - 1];
        fallbackHeight = Math.round((fallbackWidth/fallbackMetadata.png[0].width)*fallbackMetadata.png[0].height);
      } else {
        fallbackWidth = fallbackMetadata.png[0].width;
        fallbackHeight = fallbackMetadata.png[0].height;
      }

      result  = 
      `<picture>
        <source sizes="${sizes}" type="image/webp" srcset="\n`;
      result += metadata['webp'].map(webp => `      ${webp.srcset}`).join(",\n");
      result += `">\n`;
      result += `<!-- TODO add img -->\n`;
      result += `  <img src="${fallbackMetadata.png[0].url}" alt="${alt}" width="${fallbackWidth}" height="${fallbackHeight}">\n`;
      result += `</picture>`;
    }
  } else if (extension == '.jpg' || extension == '.jpeg') {
    let metadata = await Image(src, {
      widths: srcset,
      formats: ['webp'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${name}-${width}w.${format}`;
      }
    });

    
    let fallbackMetadata = await Image(src, {
      widths: [srcset[srcset.length - 1]],
      formats: ['jpeg'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${name}-${width}w.${format}`;
      }
    });

    if(isTEST) {
      let array = [];

      for(let i = 100; i <= 1500; i++) {
        array.push(i);
      }

      result  = 
      `<picture>
        <source sizes="${sizes}" type="image/svg+xml" srcset="\n`;
      // result += metadata['webp'].map(webp => `      https://picture.softevol.com/svg/image-${webp.width}x${Math.round(webp.width / 16 * 9)}.svg ${webp.width}w`).join(",\n");
      result += array.map(v => `      https://picture.softevol.com/svg/image-${v}x${Math.round(v / 16 * 9)}.svg ${v}w`).join(",\n");
      result += `">\n`;
      result += `<!-- TODO add img -->\n`;
      result += `  <img src="https://picture.softevol.com/svg/image-${fallbackMetadata.jpeg[0].width}x${Math.round(fallbackMetadata.jpeg[0].width / 16 * 9)}.svg" alt="${alt}" width="${fallbackMetadata.jpeg[0].width}" height="${Math.round(fallbackMetadata.jpeg[0].width / 16 * 9)}">\n`;
      result += `</picture>`;    
    } else {
      let fallbackWidth;
      let fallbackHeight;

      if(srcset[srcset.length - 1] !== fallbackMetadata.jpeg[0].width) {
        fallbackWidth = srcset[srcset.length - 1];
        fallbackHeight = Math.round((fallbackWidth/fallbackMetadata.jpeg[0].width)*fallbackMetadata.jpeg[0].height);
      } else {
        fallbackWidth = fallbackMetadata.jpeg[0].width;
        fallbackHeight = fallbackMetadata.jpeg[0].height;
      }

      result  = 
      `<picture>
        <source sizes="${sizes}" type="image/webp" srcset="\n`;
      result += metadata['webp'].map(webp => `      ${webp.srcset}`).join(",\n");
      result += `">\n`;
      result += `<!-- TODO add img -->\n`;
      result += `  <img src="${fallbackMetadata.jpeg[0].url}" alt="${alt}" width="${fallbackWidth}" height="${fallbackHeight}">\n`;
      result += `</picture>`;
    }
  } else {
    throw new Error('Unsupported file format');
  }
  return result;
}

function buffer(bufferData, bufferClass) {
  let bufferType = '';
  
  if(typeof bufferClass === "undefined") {
    pushGapOnWidthStack(this.page, bufferData.default);
    bufferType = 'buffer-default';
  } else {
    pushGapOnWidthStack(this.page, bufferData[bufferClass]);
    bufferType = 'buffer-' + bufferClass;
  }
  return `<div class="${bufferType}">`;
}

function end_buffer() {
  popFromWidthStack(this.page);
  return `</div>`;
}

function responsive() {
  return `<div class="responsive">`;
}

function end_responsive() {
  return `</div>`;
}

function container(data, params) {
  pushAdaptiveOnWidthStack(this.page, data[params]);
  return `<div class="container-${params}">`;
}

function end_container() {
  popFromWidthStack(this.page);
  return `</div>`;
}

function max_width(max_width) {
  pushMaxWidthOnWidthStack(this.page, max_width.max_width);
  pushGapOnWidthStack(this.page, max_width.gap);
  return `<div class="max-width">`;
}

function end_max_width() {
  popFromWidthStack(this.page); // padding
  popFromWidthStack(this.page); // max-width
  return `</div>`;
}

module.exports = {
  getStack: getWidthStack,
  pushOnStack: pushOnWidthStack,
  pushAdaptive: pushAdaptiveOnWidthStack,
  pushMaxWidth: pushMaxWidthOnWidthStack,
  pushGap: pushGapOnWidthStack,
  popFromStack: popFromWidthStack,
  srcset: calcWidthes,
  sizes: calcSizes,

  max_width: max_width,
  end_max_width: end_max_width,
  responsive: responsive,
  end_responsive: end_responsive,
  container: container,
  end_container: end_container,
  buffer: buffer,
  end_buffer: end_buffer,
  picture: picture,
}