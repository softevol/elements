function getAccordionCounter(page) {
  if (!page.accordionCounter) {
    page.accordionCounter = 0;
  }
  return page.accordionCounter;
}

function incrementAccorionCounter(page) {
  getAccordionCounter(page);
  page.accordionCounter++;
  return page.accordionCounter;
}

function accordion(data) {
  const template = `
  <div class="accordion">
  <div class="accordion__inner">
  ${data}
  </div>
  </div>`;

  return template;
}

function accordion_title(data, title, status) {
  const counter = incrementAccorionCounter(this.page);
  let titleClassname = 'accordion__title';
  let toggleClassname = 'accordion__toggle';
  let indicatorTemplate = '';
  if(status === 'active') {
    titleClassname += ' accordion__title--active';
    toggleClassname += ' accordion__toggle--active'
  }
  if(typeof data['indicator'] !== 'undefined' && Object.keys(data.indicator).length != 0) {
    titleClassname += ' accordion__title--indicator';
    indicatorTemplate = '<div class="accordion__indicator"></div>'
  }

const template = `
<h3 class="${titleClassname}">
<button class="${toggleClassname}" type="button" disabled="disabled" aria-expanded="true" aria-controls="accordion-panel-${counter}" id="accordion-toggle-${counter}">
<span class="h3">
${title}
</span>
${indicatorTemplate}
</button>
</h3>`

  return template;
}

function accordion_panel(data, status) {
  const counter = getAccordionCounter(this.page);
  let panelClassname = 'accordion__panel';

  if(status === 'active') {
    panelClassname += ' accordion__panel--active';
  }

const template = `
<div class="${panelClassname}" id="accordion-panel-${counter}" role="region" aria-labelledby="accordion-control-${counter}">
<div class="accordion__content">
${data}
</div>
</div>
`;

  return template;
}


module.exports = {
  accordion: accordion,
  title: accordion_title,
  panel: accordion_panel
}