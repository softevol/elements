---
layout: layouts/documentation.njk
title: Header
---

{% max_width max_width %}

# Header

## Configuration

In data folder (by default `_data/`) create file `site.json`. There define your project name:

``` js
{
  "name": "softevol elements"
}
```

In data folder create file `menu.js`. There define your header links and `breakpoint` for phone version

``` js
const responsive = require('./responsive.json');

module.exports = {
    links : {
        "Typography": "/design-system/typography/",
        "Color System": "/design-system/color-system/",
        "UI Kit": "/design-system/ui-kit/",
        "Assets": "/design-system/assets/"
    },
    breakpoint: responsive.MEDIA.phone
}
```

You can set own value for `breakpoint`, or define `MEDIA` in `_data/responsive.json` and use one of them:

``` js
...
{
  "MEDIA": {
    "phone": 600,
    "tablet": 905,
    "laptop": 1240,
    "desktop": 1440
  },
}
...
```

## Usage

In includes folder (by default `_includes`), create file `partials/header.njk`

``` md
{ % include "../../node_modules/elements/_includes/partials/header.njk" % }
```

Create file `css/header/header.css.ejs`, and there include css styles for header:
``` css
---
permalink: css/header.css
---

<%- include("../../node_modules/elements/_includes/css/header/simple/ie5.css.ejs"); %>
<%- include("../../node_modules/elements/_includes/css/header/simple/ie7.css"); %>
<%- include("../../node_modules/elements/_includes/css/header/simple/ie8.css"); %>
<%- include("../../node_modules/elements/_includes/css/header/simple/mobile.css.ejs"); %>
<%- include("../../node_modules/elements/_includes/css/header/simple/flex.css.ejs"); %>
``` 

Also, you can add links and burger styling:

``` css
...
<%- include("../../node_modules/elements/_includes/css/menu/main-menu__link.css.ejs"); %>
<%- include("../../node_modules/elements/_includes/css/menu/main-menu__link--current.css.ejs"); %>
<%- include("../../node_modules/elements/_includes/css/menu/burger-button.css.ejs"); %>
...
``` 

Create file `img/buttons/burger-base64.svg.ejs`:

```md
PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTIiIHZpZXdCb3g9IjAgMCAxOCAxMiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4NCiAgICA8cGF0aCBkPSJNMCAxMkgxOFYxMEgwVjEyWk0wIDdIMThWNUgwVjdaTTAgMFYySDE4VjBIMFoiIGZpbGw9IiMyRDJEMkQiLz4NCjwvc3ZnPg0K
```

Create file `cross-base64.svg.ejs`:

```md
PHN2ZyB3aWR0aD0iMTQiIGhlaWdodD0iMTQiIHZpZXdCb3g9IjAgMCAxNCAxNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4NCjxwYXRoIGQ9Ik0xNCAxLjQxTDEyLjU5IDBMNyA1LjU5TDEuNDEgMEwwIDEuNDFMNS41OSA3TDAgMTIuNTlMMS40MSAxNEw3IDguNDFMMTIuNTkgMTRMMTQgMTIuNTlMOC40MSA3TDE0IDEuNDFaIiBmaWxsPSIjMkQyRDJEIi8+DQo8L3N2Zz4NCg==
```

Create file `js/utils.js.ejs`, and there include utils js for header:

``` js
...
<%- include("../node_modules/elements/_includes/js/utils/class-function.js"); %>
<%- include("../node_modules/elements/_includes/js/utils/window-sizes.js"); %>
<%- include("../node_modules/elements/_includes/js/utils/is-media-queries-supported.js"); %>
<%- include("../node_modules/elements/_includes/js/utils/support-css.js"); %>
<%- include("../node_modules/elements/_includes/js/utils/keycodes.js"); %>
...
``` 

Create file `js/header.js.ejs`, and there include js for header:

``` js
...
<%- include("../node_modules/elements/_includes/js/header/menu.js.ejs"); %>
<%- include("../node_modules/elements/_includes/js/header/mobile.js.ejs"); %>
<%- include("../node_modules/elements/_includes/js/header/anchor-links.js"); %>
<%- include("../node_modules/elements/_includes/js/header/close-dropdown-main-menu.js.ejs"); %>
<%- include("../node_modules/elements/_includes/js/header/empty-space-click.js"); %>
<%- include("../node_modules/elements/_includes/js/header/keyboard-support.js"); %>
...
``` 

{% end_max_width %}