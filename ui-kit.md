---
layout: layouts/documentation.njk
title: UI Kit
---

{% max_width max_width %}

# UI Kit

## Design system

In includes folder (by default `_includes/`) create file `layouts/design-system/ui-kit.njk`, and there include template from `elements` module:

``` md

{ % include "../../../node_modules/elements/_includes/layouts/design-system/ui-kit.njk" % }
```

At the root directory of project create folder `design-system`, and there create file `ui-kit.md` :

``` md
  ---
    layout: layouts/design-system/ui-kit.njk
    title: UI Kit
  ---
```

## Include design system

You need add `ui_kit_block` shortcode with config and styles.

In Eleventy config file (by default `.eleventy.js`) register shortcode:
``` js
...
const elements = require("elements");
...
module.exports = function (eleventyConfig) {
  ...
  eleventyConfig.addShortcode("ui_kit_block", elements.designSystem.ui_kit_block);
  ...
}
```

Create file `css/design-system/ui-kit.css.ejs`, and there write next:

``` css
---
permalink: css/design-system/ui-kit.css
---

<%- include("../../node_modules/elements/_includes/css/design-system/ui-kit.css.ejs"); %>
``` 

Also, you need add `max_width/end_max_width` with config and styles.
Read more about [`max_width`](/responsive-layout/#max_width).


## Usage


`ui_kit_block` have three params:

``` md
{ % ui_kit_block containers, name, type % }
```
`containers` - obj with containers data  
`name` - class for ui-kit element and it's name  
`type` - type of ui-kit elem. Currently 2 supported: link and button  


Use `ui_kit_block`  in `design-system/ui-kit.md` file:

``` md

### main-menu__link

{ % responsive % }
{ % ui_kit_block "main-menu__link", "link" % }
{ % end_responsive % }

### burger-button

{ % responsive % }
{ % ui_kit_block "burger-button", "button" % }
{ % end_responsive % }

```
{% end_max_width %}
